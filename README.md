# DOCUMINE

### ABOUT

Data mining project for all kinds of unstructured text.
Currently only language guessing is available. 

### GETTING STARTED
Run with:

`./gradlew bootRun`                              // default -> production

`./gradlew bootRun -Dspring.profiles.active=dev` // dev

`./gradlew test`                                 // profile=test -> run integration tests and unit tests

When running as non test, go to `http://localhost:8080/swagger-ui.html`
to view api documentation.

Needs postgres server running (from  9.4) and schema `documine`,`documine_dev` and
`documine_test` installed.

### EMAIL SENDING
Emails are send via gmail-smtp.
You need to set client_access credentials and a gmail account.
See properties `mail.smtp.user` and `mail.smtp.pw`.
In case you don't want to send emails, use flag 
`mail.sending.enabled=false`




