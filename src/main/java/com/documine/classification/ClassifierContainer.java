package com.documine.classification;

import com.documine.exception.ClassifierException;
import com.documine.web.services.classification.Helpers;
import com.documine.web.webmodels.response.classification.ClassAndConfidenceModel;
import com.documine.web.webmodels.response.classification.ClassificationResultModel;
import com.google.common.collect.Lists;
import weka.classifiers.AbstractClassifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

public class ClassifierContainer {

    private Instances trainingData;
    private List<String> classNames;
    private AbstractClassifier classifier;
    private StringToWordVector filter;
    private String metricsName;

    public ClassifierContainer(
            final String trainingDataResourcePath,
            final String classifierModelResourcePath,
            final String filterResourcePath,
            final String classAttrName,
            final String metricsName) throws IOException, ClassNotFoundException {
        trainingData = loadTrainingData(trainingDataResourcePath);
        classNames = retrieveClassNames(classAttrName);
        classifier = createClassifier(classifierModelResourcePath);
        filter = loadFilter(filterResourcePath);
        this.metricsName = metricsName;
    }

    public List<String> getClassNames() {
        return classNames;
    }


    public int getNumAttributes() {
        return trainingData.numAttributes();
    }

    public Attribute getAttribute(final String attributeName) {
        return trainingData.attribute(attributeName);
    }

    public ClassificationResultModel guessClassConfidences(final String sample) throws ClassifierException {
        try {
            final Instance instanceToClassify = preprocess(sample);
            final double[] confidences = classifier.distributionForInstance(instanceToClassify);
            return toModel(confidences, sample);
        } catch (final Exception e) {
            throw new ClassifierException(e.getMessage());
        }
    }

    private ClassificationResultModel toModel(final double[] confidences, final String sample) {
        final List<ClassAndConfidenceModel> results = Lists.newArrayList();
        for (int i = 0; i < confidences.length; i++) {
            results.add(new ClassAndConfidenceModel(getClassNames().get(i), confidences[i]));
        }

        final List<ClassAndConfidenceModel> sortedResults = results.stream().sorted((a, b) ->
                Double.compare(b.confidence, a.confidence)).collect(Collectors.toList());
        final ClassificationResultModel model = new ClassificationResultModel();
        model.classificationResult = sortedResults;
        model.bestGuess = sortedResults.get(0);
        model.inputSnippet = Helpers.createSnippet(sample);
        model.metrics = metricsName;
        return model;
    }

    private Instance preprocess(final String sample) throws Exception {
        final Instance raw = toInstances(sample); // Instance with string attr for sample an a nominal attr for class
        final Instance vector = rawToVector(raw);
        return makeClassifierCompatible(vector);
    }

    private Instance toInstances(final String testSample) {
        final List<Attribute> attributes = createAttributes();
        final Attribute textAttribute = attributes.get(0);
        final Attribute classAttribute = attributes.get(1);
        final Instances dataset = createDataSet(attributes, classAttribute);
        final Instance raw = new DenseInstance(2);
        raw.setDataset(dataset);
        raw.setValue(textAttribute, textAttribute.addStringValue(testSample)); // this is the way to set string attribute values
        raw.setValue(classAttribute, "?");
        return raw;
    }

    private List<Attribute> createAttributes() {
        // Create nominal attribute "position"
        final Attribute textAttribute = new Attribute("text", (ArrayList) null); // WTF!!! this is the way WEKA defines this as a string attribute
        final Attribute classAttribute = new Attribute("class", Lists.newArrayList("?"));
        return Lists.newArrayList(textAttribute, classAttribute);
    }

    private Instances createDataSet(final List<Attribute> attributes, final Attribute classAttribute) {
        // TODO: WTF - why is an array list expected here !!!
        final Instances dataset = new Instances("dataset", (ArrayList) attributes, 0);
        dataset.setClassIndex(classAttribute.index());
        return dataset;
    }

    private Instance rawToVector(final Instance raw) throws Exception {
        // TODO: this is not thread safe
        filter.setAttributeIndices("first");
        filter.setInputFormat(raw.dataset());
        Instances inst = new Instances(raw.dataset(), 1);
        inst.add(raw);
        Instances filtered = Filter.useFilter(inst, filter);
        return filtered.get(0);
    }

    private Instance makeClassifierCompatible(final Instance incompatible) {

        final Instance compatibleInstance = new DenseInstance(getNumAttributes());

        final Enumeration<Attribute> attributes = incompatible.enumerateAttributes();
        while (attributes.hasMoreElements()) {
            final Attribute currentAttribute = attributes.nextElement();
            final Attribute textAttribute = getAttribute(currentAttribute.name());

            if (textAttribute != null) {
                final double currentAttrValue = incompatible.value(currentAttribute);
                compatibleInstance.setValue(textAttribute, currentAttrValue);
            }
        }
        return compatibleInstance;
    }

    private <T> T loadFilter(final String filterResourcePath) throws IOException, ClassNotFoundException {
        try (final FileInputStream fis = new FileInputStream(filterResourcePath)) {
            final T filter = (T) (new ObjectInputStream(fis)).readObject();
            return filter;
        }
    }

    private Instances loadTrainingData(final String trainingDataResourcePath) throws IOException {
        try (final BufferedReader reader = new BufferedReader(new FileReader(trainingDataResourcePath))) {
            return trainingData = new Instances(reader);
        }
    }

    private List<String> retrieveClassNames(final String classAttributeName) {
        final Attribute attr = trainingData.attribute(classAttributeName);
        if (attr == null)
            throw new RuntimeException("attr name '" + classAttributeName + "' could not be found in training data");

        final List<String> classNames = Lists.newArrayList();
        final Enumeration<Object> values = attr.enumerateValues();
        while (values.hasMoreElements()) {
            classNames.add(values.nextElement().toString());
        }
        return classNames;
    }

    private <T> T createClassifier(final String classifierModelResourcePath) throws IOException, ClassNotFoundException {
        try (final FileInputStream fis = new FileInputStream(classifierModelResourcePath)) {
            final T classifier = (T) (new ObjectInputStream(fis)).readObject();
            return classifier;
        }
    }


}
