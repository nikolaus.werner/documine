package com.documine.sql.services;

import com.documine.sql.models.AuthToken;
import com.documine.sql.models.UserAccount;
import com.documine.sql.repositories.AuthTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuthTokenService {

    @Autowired
    private AuthTokenRepository authTokenRepository;

    public void deleteByTokenValue(final String tokenValue) {
        final AuthToken authToken = authTokenRepository.getByToken(tokenValue);
        authTokenRepository.delete(authToken);
    }

    public void deleteByUserAccount(final UserAccount userAccount) {
        authTokenRepository.deleteByUserAccount(userAccount);
    }
}
