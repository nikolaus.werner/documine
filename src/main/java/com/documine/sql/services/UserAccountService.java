package com.documine.sql.services;

import com.documine.sql.models.AuthToken;
import com.documine.sql.models.UserAccount;
import com.documine.sql.repositories.AuthTokenRepository;
import com.documine.sql.repositories.UserAccountRepository;
import com.documine.web.webmodels.request.AuthenticationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserAccountService {

    @Autowired
    private AuthTokenRepository authTokenRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    public UserAccount getByToken(final String token) {
        final AuthToken authToken = authTokenRepository.getByToken(token);
        return authToken != null ? authToken.getUserAccount() : null;
    }

    public UserAccount getByEmail(final String email) {
        return userAccountRepository.getByEmail(email);
    }


    public UserAccount saveNewAccount(final AuthenticationModel model) {
        final UserAccount userAccount = new UserAccount();
        userAccount.setPassword(model.password);
        userAccount.setEmail(model.email.toLowerCase());
        return userAccountRepository.save(userAccount);
    }

    public UserAccount verify(final UserAccount userAccount) {
        userAccount.setVerifiedEmail(true);
        return userAccountRepository.save(userAccount);
    }
}
