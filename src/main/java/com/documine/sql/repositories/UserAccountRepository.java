package com.documine.sql.repositories;


import com.documine.sql.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    public UserAccount getByEmail(final String email);
}
