package com.documine.sql.repositories;


import com.documine.sql.models.AuthToken;
import com.documine.sql.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface AuthTokenRepository extends JpaRepository<AuthToken, Long> {

    public AuthToken getByToken(final String token);

    @Modifying
    @Query("DELETE FROM AuthToken a WHERE a.userAccount=:userAccount")
    public void deleteByUserAccount(@Param("userAccount") final UserAccount userAccount);
}
