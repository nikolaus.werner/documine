package com.documine.sql.models;

import com.documine.sql.events.AuthTokenListener;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(uniqueConstraints =
@UniqueConstraint(columnNames = {"user_account_id", "token"}))
@EntityListeners(AuthTokenListener.class)
public class AuthToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column
    private Date creationTime;

    @Column
    private String token;

    @ManyToOne(fetch = FetchType.LAZY) // we always want all user information when querying by token
    private UserAccount userAccount;

    public AuthToken() {
    }

    public AuthToken(final UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public String getToken() {
        return token;
    }

    public void setUserAccount(final UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public void setToken(final String token) {
        this.token = token;
    }

}
