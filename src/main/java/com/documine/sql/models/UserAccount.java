package com.documine.sql.models;

import com.documine.sql.events.UserAccountListener;
import com.documine.web.authentication.Role;
import org.hibernate.annotations.CreationTimestamp;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
@EntityListeners(UserAccountListener.class)
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column
    private Date creationTime;

    @NotNull
    @Column
    private boolean verifiedEmail = false;

    @Column(unique = true)
    private String email;

    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private String passwordHash;

    @Column
    private String verificationToken;

    @OneToMany(fetch = FetchType.LAZY)
    private final List<Project> projects = new ArrayList<>();

    public UserAccount() {
    }

    public UserAccount(final String email) {
        this.email = email;
    }

    public void setPassword(final String password) {
        passwordHash = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public boolean passwordMatches(final String password) {
        return BCrypt.checkpw(password, passwordHash);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setVerificationToken(final String verificationToken) {
        this.verificationToken = verificationToken;
    }

    public void setVerifiedEmail(boolean verifiedEmail) {
        this.verifiedEmail = verifiedEmail;
    }

    public String getEmail() {
        return this.email;
    }

    public boolean hasVerifiedEmail() {
        return this.verifiedEmail;
    }

    public Date getCreationTime() {
        return this.creationTime;
    }

    public Long getId() {
        return this.id;
    }

    public String getVerificationToken() {
        return this.verificationToken;
    }

}
