package com.documine.sql.events;

import com.documine.sql.models.UserAccount;
import com.documine.web.authentication.Role;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.UUID;

public class UserAccountListener {

    @PrePersist
    public void genVerificationToken(final UserAccount userAccount) {
        userAccount.setVerificationToken(Base64.encodeBase64URLSafeString(UUID.randomUUID().toString().getBytes()));
        doWhenVerified(userAccount);
    }

    @PreUpdate
    public void doWhenVerified(final UserAccount userAccount) {
        // when verification is done, verification token should be deleted
        if (userAccount.hasVerifiedEmail() == true)
            userAccount.setRole(Role.VERIFIED_USER);
    }
}
