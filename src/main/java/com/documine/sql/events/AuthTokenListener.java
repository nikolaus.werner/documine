package com.documine.sql.events;

import com.documine.sql.models.AuthToken;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.PrePersist;
import java.util.UUID;

public class AuthTokenListener {

    @PrePersist
    public void genToken(final AuthToken authToken) {
        authToken.setToken(Base64.encodeBase64String(UUID.randomUUID().toString().getBytes()));
    }
}
