package com.documine.config;

import com.documine.classification.ClassifierContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class ClassifierConfig {
    @Bean(name = "languageClassifier")
    public ClassifierContainer getLanguageClassifierContainer() throws IOException, ClassNotFoundException {
        final ClassifierContainer classifier = new ClassifierContainer(
                "src/main/resources/lgClassification/LanguageTraining_2gram_f300.arff",
                "src/main/resources/lgClassification/logisticClassifier_v1.model",
                "src/main/resources/lgClassification/CharNGramTokenizer",
                "language",
                "LANGUAGE");
        return classifier;
    }


    @Bean(name = "userAgentClassifier")
    public ClassifierContainer getUserAgentClassifierContainer() throws IOException, ClassNotFoundException {
        final ClassifierContainer classifier = new ClassifierContainer(
                "src/main/resources/lgClassification/LanguageTraining_2gram_f300.arff",
                "src/main/resources/lgClassification/logisticClassifier_v1.model",
                "src/main/resources/lgClassification/CharNGramTokenizer",
                "language",
                "UA...");
        return classifier;
    }
}
