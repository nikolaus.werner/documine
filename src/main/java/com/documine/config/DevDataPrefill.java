package com.documine.config;

import com.documine.sql.models.UserAccount;
import com.documine.sql.repositories.UserAccountRepository;
import com.documine.web.authentication.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"dev"})
public class DevDataPrefill implements ApplicationRunner {

    @Autowired
    private UserAccountRepository userRepository;

    public void run(ApplicationArguments args) {
        addDevUserAccount("dev_admin@documine.com", "§admin");
    }


    private void addDevUserAccount(final String email, final String pw) {
        if (userRepository.getByEmail(email) == null) {
            final UserAccount devUser = new UserAccount(email);
            devUser.setPassword(pw);
            devUser.setRole(Role.ADMIN);
            userRepository.save(devUser);
        }
    }
}