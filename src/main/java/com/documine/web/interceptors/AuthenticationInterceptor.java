package com.documine.web.interceptors;

import com.documine.exception.ForbiddenException;
import com.documine.exception.NotFoundException;
import com.documine.web.authentication.Grant;
import com.documine.web.services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationInterceptor.class);

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public boolean preHandle(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final Object handler) throws Exception {
        final String controllerName = getControllerName(handler);
        final String actionName = getActionName(handler);
        final Grant grant = authenticationService.getControllerGrant(controllerName, actionName);

        if (grant == null) {
            LOGGER.info("No grants for " + request.getRequestURL() + " found!");
            throw new NotFoundException();
        }

        if (!authenticationService.isAllowed(grant))
            throw new ForbiddenException();

        return true;
    }

    private String getControllerName(final Object handler) {
        final HandlerMethod handlerMethod = castHandler(handler);
        return handlerMethod != null ? handlerMethod.getBeanType().getSimpleName().replace("Controller", "").toLowerCase() : null;
    }

    private String getActionName(final Object handler) {
        final HandlerMethod handlerMethod = castHandler(handler);
        return handlerMethod != null ? handlerMethod.getMethod().getName().toLowerCase() : null;
    }

    private HandlerMethod castHandler(Object handler) {
        if (handler instanceof HandlerMethod)
            return (HandlerMethod) handler;
        return null;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // do nothing
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // do nothing
    }
}
