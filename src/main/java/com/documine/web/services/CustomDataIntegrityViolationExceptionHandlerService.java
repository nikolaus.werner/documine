package com.documine.web.services;

import com.documine.exception.InputException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CustomDataIntegrityViolationExceptionHandlerService {

    private static final Pattern UNIQUE_KEY_VIOLATION_ELEMENT = Pattern.compile(".*\\(([^\\)]+)\\)=.*");
    private static final Pattern UNIQUE_KEY_VIOLATION_CAUSE = Pattern.compile(".*unique.*constraint.*", Pattern.CASE_INSENSITIVE);

    // TODO: very fragile approach to identify uniqueness violations
    public boolean isUniqueKeyViolation(final DataIntegrityViolationException ex) {
        final String cause = ex.getMostSpecificCause().toString();
        return UNIQUE_KEY_VIOLATION_CAUSE.matcher(cause).find();
    }

    public String getUniqueKeyViolationElement(final DataIntegrityViolationException ex) {
        final String cause = ex.getMostSpecificCause().toString();
        final Matcher matcher = UNIQUE_KEY_VIOLATION_ELEMENT.matcher(cause);
        if (matcher.find())
            return matcher.group(1);
        return null;
    }

    public InputException toInputException(final DataIntegrityViolationException ex) {
        final String key = getUniqueKeyViolationElement(ex);
        final Map<String, String> invalidFields = new HashMap<>();
        invalidFields.put(key, "already_exists");
        return new InputException(invalidFields);
    }

}
