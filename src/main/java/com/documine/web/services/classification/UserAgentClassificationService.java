package com.documine.web.services.classification;

import com.documine.classification.ClassifierContainer;
import com.documine.exception.ClassifierException;
import com.documine.web.webmodels.response.classification.ClassAndConfidenceModel;
import com.documine.web.webmodels.response.classification.ClassificationResultModel;
import com.documine.web.webmodels.response.classification.MultiClassificationResultModel;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserAgentClassificationService {

    @Autowired
    @Qualifier("userAgentClassifier")
    private ClassifierContainer classifier;

    public MultiClassificationResultModel guessUserAgent(final String input) throws ClassifierException {

        // TODO: integrate classifier
        final ClassificationResultModel mobile = new ClassificationResultModel();
        mobile.classificationResult = Lists.newArrayList(
                new ClassAndConfidenceModel("true", 0.7),
                new ClassAndConfidenceModel("false", 0.3));
        mobile.bestGuess = new ClassAndConfidenceModel("isMobile", 0.7);
        mobile.metrics = "UA-is-mobile";

        final ClassificationResultModel device = new ClassificationResultModel();
        mobile.classificationResult = Lists.newArrayList(
                new ClassAndConfidenceModel("Mobile Phone", 0.5),
                new ClassAndConfidenceModel("Desktop", 0.3));
        mobile.bestGuess = new ClassAndConfidenceModel("Mobile Phone", 0.5);
        device.metrics = "UA-device";

        final ClassificationResultModel browserType = new ClassificationResultModel();
        mobile.classificationResult = Lists.newArrayList(
                new ClassAndConfidenceModel("Browser", 0.4),
                new ClassAndConfidenceModel("EmailClient", 0.3),
                new ClassAndConfidenceModel("Crawler", 0.2),
                new ClassAndConfidenceModel("Feed Reader", 0.1));
        mobile.bestGuess = new ClassAndConfidenceModel("Browser", 0.4);
        browserType.metrics = "UA-browser-type";

        // TODO: cache results in DB to have same results for old guesses, also when classifier is retrained
        final MultiClassificationResultModel model = new MultiClassificationResultModel(
                Lists.newArrayList(mobile,
                        device,
                        browserType)
        );
        model.inputSnippet = Helpers.createSnippet(input);
        return model;

    }

}
