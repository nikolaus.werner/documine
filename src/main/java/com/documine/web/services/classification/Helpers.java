package com.documine.web.services.classification;

public class Helpers {

    public static String createSnippet(final String content) {
        return content.length() < 40 ? content : content.substring(0, 40) + "...";
    }
}
