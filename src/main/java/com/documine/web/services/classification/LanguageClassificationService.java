package com.documine.web.services.classification;

import com.documine.classification.ClassifierContainer;
import com.documine.exception.ClassifierException;
import com.documine.web.webmodels.response.classification.ClassificationResultModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class LanguageClassificationService {

    @Autowired
    @Qualifier("languageClassifier")
    private ClassifierContainer classifier;

    public ClassificationResultModel guessLanguage(final String input) throws ClassifierException {
        return classifier.guessClassConfidences(input);
    }


}
