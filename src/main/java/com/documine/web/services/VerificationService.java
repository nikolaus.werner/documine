package com.documine.web.services;

import com.documine.exception.NotFoundException;
import com.documine.sql.models.UserAccount;
import com.documine.sql.services.UserAccountService;
import com.documine.web.webmodels.response.SuccessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class VerificationService {

    @Autowired
    private UserAccountService userAccountService;

    public ResponseEntity<SuccessModel> verify(final String token, final String email) throws NotFoundException {
        final UserAccount ua = userAccountService.getByEmail(email);
        if (ua == null)
            throw new NotFoundException("email not found");

        if (ua.getVerificationToken() == null && !ua.getVerificationToken().equals(token))
            // return this exception, so it is not possible to find out email addresses by
            // exploiting the endpoint
            throw new NotFoundException("email not found");

        if (ua.hasVerifiedEmail())
            return new ResponseEntity<SuccessModel>(new SuccessModel("already verified"), new HttpHeaders(), HttpStatus.OK);

        userAccountService.verify(ua);
        return new ResponseEntity<SuccessModel>(new SuccessModel(), new HttpHeaders(), HttpStatus.OK);
    }
}
