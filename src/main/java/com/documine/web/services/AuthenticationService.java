package com.documine.web.services;

import com.documine.exception.AuthenticationException;
import com.documine.exception.NotFoundException;
import com.documine.sql.models.AuthToken;
import com.documine.sql.models.UserAccount;
import com.documine.sql.repositories.AuthTokenRepository;
import com.documine.sql.services.UserAccountService;
import com.documine.web.authentication.ControllerGrantService;
import com.documine.web.authentication.Grant;
import com.documine.web.authentication.Role;
import com.documine.web.webmodels.request.AuthenticationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

import static com.documine.web.Constants.X_AUTH_TOKEN;

@Service
@Transactional
public class AuthenticationService {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private ControllerGrantService controllerGrant;

    @Autowired
    private AuthTokenRepository authTokenRepository;

    public String getToken() {
        final String token = request.getHeader(X_AUTH_TOKEN);
        return token;
    }

    public boolean isAllowed(final Grant grant) {
        final Role role = getRole();
        return role.hasGrant(grant);
    }

    public Grant getControllerGrant(final String controllerName, final String actionName) {
        return controllerGrant.getControllerGrant(controllerName, actionName);
    }

    public String createNewToken(final UserAccount userAccount) {
        final AuthToken authToken = authTokenRepository.save(new AuthToken(userAccount));
        return authToken.getToken();
    }

    public UserAccount getUserWithCredentials(final AuthenticationModel model) throws Exception {
        final UserAccount userAccount = userAccountService.getByEmail(model.email);
        if (userAccount == null) {
            throw new NotFoundException("unknown user", "unknown_user");
        }
        if (!userAccount.passwordMatches(model.password)) {
            throw new AuthenticationException();
        }
        return userAccount;
    }

    private Role getRole() {
        final UserAccount u = getUserAccount();
        if (u != null && u.getRole() != null)
            return u.getRole();
        return Role.ANONYMOUS;
    }

    public UserAccount getUserAccount() {
        final String token = getToken();
        return userAccountService.getByToken(token);
    }

}
