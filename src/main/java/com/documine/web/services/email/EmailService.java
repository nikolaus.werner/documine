package com.documine.web.services.email;

public interface EmailService {

    public void sendVerificationMail(final String email, final String verificationToken);
}
