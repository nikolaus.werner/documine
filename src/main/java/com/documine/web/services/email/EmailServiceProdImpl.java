package com.documine.web.services.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Profile({"default", "dev"})
public class EmailServiceProdImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceProdImpl.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${mail.verification.page}")
    private String verificationPage;

    @Value("${mail.sending.enabled}")
    private Boolean sendingEnabled;

    // TODO: Do this with an email templating service like mailchimp
    @Override
    public void sendVerificationMail(final String emailAddress, final String verificationToken) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(verificationPage + "/" + verificationToken)
                .queryParam("email", emailAddress);

        final String link = builder.build().encode().toUriString();
        LOGGER.info(link);
        if (sendingEnabled) {
            final SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailAddress);
            message.setSubject("Verify your documine account");
            message.setText(link);
            javaMailSender.send(message);
        }
    }


}
