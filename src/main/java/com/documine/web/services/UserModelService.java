package com.documine.web.services;

import com.documine.sql.models.UserAccount;
import com.documine.web.webmodels.response.UserAccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserModelService {

    @Autowired
    private AuthenticationService authenticationService;

    public UserAccountModel asModel(final UserAccount account) {
        final UserAccountModel userModel = new UserAccountModel();
        userModel.setEmail(account.getEmail());
        userModel.setVerified(account.hasVerifiedEmail());
        userModel.setCreatedAt(account.getCreationTime());
        userModel.setRole(account.getRole());
        userModel.setResourceAccess(account.getRole().getGrants());
        return userModel;

    }
}
