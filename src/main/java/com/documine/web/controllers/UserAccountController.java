package com.documine.web.controllers;

import com.documine.sql.models.UserAccount;
import com.documine.web.services.AuthenticationService;
import com.documine.web.services.UserModelService;
import com.documine.web.webmodels.response.UserAccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserAccountController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserModelService userModelService;

    @RequestMapping(value = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UserAccountModel> showMe() {
        final UserAccount account = authenticationService.getUserAccount();
        final UserAccountModel respBody = userModelService.asModel(account);
        return new ResponseEntity<UserAccountModel>(respBody, new HttpHeaders(), HttpStatus.OK);
    }
}
