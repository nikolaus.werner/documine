package com.documine.web.controllers;


import com.documine.exception.InputException;
import com.documine.exception.NotFoundException;
import com.documine.sql.models.UserAccount;
import com.documine.sql.services.AuthTokenService;
import com.documine.sql.services.UserAccountService;
import com.documine.web.services.AuthenticationService;
import com.documine.web.services.VerificationService;
import com.documine.web.services.email.EmailService;
import com.documine.web.webmodels.request.AuthenticationModel;
import com.documine.web.webmodels.request.LogoutModel;
import com.documine.web.webmodels.request.VerificationModel;
import com.documine.web.webmodels.response.SuccessModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.documine.web.Constants.X_AUTH_TOKEN;

@Controller
public class AuthenticationController {

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private VerificationService verificationService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private AuthTokenService authTokenService;

    @Autowired
    private EmailService emailService;

    // TODO: verify existence of the email with external service like kickbox
    @RequestMapping(value = "/signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SuccessModel> signup(@RequestBody
                                               @Valid final AuthenticationModel model) {
        final UserAccount userAccount = userAccountService.saveNewAccount(model);
        final String token = authenticationService.createNewToken(userAccount);
        emailService.sendVerificationMail(userAccount.getEmail(), userAccount.getVerificationToken());
        final HttpHeaders headers = new HttpHeaders();
        headers.add(X_AUTH_TOKEN, token);
        return new ResponseEntity<SuccessModel>(new SuccessModel(), headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SuccessModel> login(@RequestBody
                                              @Valid final AuthenticationModel model) throws Exception {
        final UserAccount userAccount = authenticationService.getUserWithCredentials(model);
        final String token = authenticationService.createNewToken(userAccount);
        final HttpHeaders headers = new HttpHeaders();
        headers.add(X_AUTH_TOKEN, token);
        return new ResponseEntity<SuccessModel>(new SuccessModel(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/verify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SuccessModel> verifyWithPost(@RequestBody
                                                       @Valid final VerificationModel model) throws NotFoundException, InputException {
        return verificationService.verify(model.token, model.email);
    }

    @RequestMapping(value = "/verify/{token}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SuccessModel> verifyWithGet(@PathVariable("token") final String token,
                                                      @RequestParam("email") final String email) throws NotFoundException, InputException {
        return verificationService.verify(token, email);
    }


    @RequestMapping(value = "/logout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<SuccessModel> logout(@RequestBody(required = false) final LogoutModel model) {
        if (model != null && model.logoutAll) {
            authTokenService.deleteByUserAccount(authenticationService.getUserAccount());
        } else {
            authTokenService.deleteByTokenValue(authenticationService.getToken());
        }
        return new ResponseEntity<SuccessModel>(new SuccessModel(), new HttpHeaders(), HttpStatus.OK);
    }

}
