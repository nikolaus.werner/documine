package com.documine.web.controllers;

import com.documine.exception.ClassifierException;
import com.documine.web.services.classification.LanguageClassificationService;
import com.documine.web.services.classification.UserAgentClassificationService;
import com.documine.web.webmodels.request.ClassificationModel;
import com.documine.web.webmodels.response.classification.ClassificationResultModel;
import com.documine.web.webmodels.response.classification.MultiClassificationResultModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ClassificationController {

    @Autowired
    private LanguageClassificationService languageClassificationService;

    @Autowired
    private UserAgentClassificationService userAgentClassificationService;


    @RequestMapping(value = "/classify/language", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ClassificationResultModel> guessLanguage(
            @RequestBody
            @Valid ClassificationModel model) throws ClassifierException {

        return new ResponseEntity<ClassificationResultModel>(
                languageClassificationService.guessLanguage(model.contentToClassify),
                new HttpHeaders(),
                HttpStatus.OK);
    }


    @RequestMapping(value = "/classify/useragent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MultiClassificationResultModel> guessUserAgent(
            @RequestBody
            @Valid ClassificationModel model) throws ClassifierException {

        return new ResponseEntity<MultiClassificationResultModel>(
                userAgentClassificationService.guessUserAgent(model.contentToClassify),
                new HttpHeaders(),
                HttpStatus.OK);
    }

}
