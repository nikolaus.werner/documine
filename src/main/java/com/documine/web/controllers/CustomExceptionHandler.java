package com.documine.web.controllers;

import com.documine.exception.*;
import com.documine.web.services.CustomDataIntegrityViolationExceptionHandlerService;
import com.documine.web.webmodels.response.DefaultExceptionModel;
import com.documine.web.webmodels.response.InputExceptionModel;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @Autowired
    private CustomDataIntegrityViolationExceptionHandlerService customDataIntegrityViolationExceptionHandlerService;

    @ExceptionHandler({
            NotFoundException.class,
            AuthenticationException.class,
            ForbiddenException.class,
            ClassifierException.class})
    public ResponseEntity<Object> handleDocumineException(
            final HttpServletRequest req,
            final DocumineException ex) {
        LOGGER.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<Object>(
                new DefaultExceptionModel(ex),
                ex.getStatusCode());
    }


    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handlePersistenceException(
            final HttpServletRequest req,
            final DataIntegrityViolationException ex) {
        LOGGER.error("Request: " + req.getRequestURL() + " raised " + ex);
        if (customDataIntegrityViolationExceptionHandlerService.isUniqueKeyViolation(ex)) {
            final InputException iex = customDataIntegrityViolationExceptionHandlerService.toInputException(ex);
            return new ResponseEntity<Object>(
                    new InputExceptionModel(iex),
                    iex.getStatusCode());
        }
        return new ResponseEntity<Object>(
                new DefaultExceptionModel("database_exception", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    // we want to treat this, because of JSON parse Exceptions in input models
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            final HttpMessageNotReadableException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest req) {

        LOGGER.error("Request: " + req.getContextPath() + " raised " + ex);

        final DefaultExceptionModel returnedExceptionModel = new DefaultExceptionModel(ex.getMessage(), "unprocessable_entity");
        if (ex.getCause() instanceof JsonParseException) {
            returnedExceptionModel.message = "invalid json";
            returnedExceptionModel.code = "invalid_json";
        }

        return new ResponseEntity<Object>(
                returnedExceptionModel,
                HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @Override
    // We want to treat the validation exception specially
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest req) {
        LOGGER.error("Request: " + req.getContextPath() + " raised " + ex);

        final Map<String, String> invalidFields = Maps.newHashMap();

        final List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        for (final ObjectError e : errors) {
            invalidFields.put(((FieldError) e).getField(), e.getDefaultMessage());
        }
        return new ResponseEntity<Object>(
                new InputExceptionModel(invalidFields),
                HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            final NoHandlerFoundException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest req) {
        LOGGER.error("Request: " + req.getContextPath() + " raised " + ex);
        final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();
        return new ResponseEntity<Object>(new DefaultExceptionModel(error, "no_handler_found"), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({Exception.class, Error.class})
    public ResponseEntity<Object> handleGeneralException(
            HttpServletRequest req,
            HttpServletResponse resp,
            Throwable ex
    ) {
        LOGGER.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new ResponseEntity<Object>(
                new DefaultExceptionModel("internal_server_error", "Internal Server Error"),
                HttpStatus.INTERNAL_SERVER_ERROR);

    }

}

