package com.documine.web.webmodels.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogoutModel {
    @JsonProperty("logout_all")
    public boolean logoutAll = false;
}
