package com.documine.web.webmodels.request;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuthenticationModel {
    @NotNull
    @Size(min = 4)
    public String password;
    @NotNull
    @Email
    public String email;

}
