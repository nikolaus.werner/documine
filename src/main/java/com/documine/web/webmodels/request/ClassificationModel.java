package com.documine.web.webmodels.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ClassificationModel {
    @NotNull
    @JsonProperty("content_to_classify")
    public String contentToClassify;
}
