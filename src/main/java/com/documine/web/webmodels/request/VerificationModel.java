package com.documine.web.webmodels.request;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

public class VerificationModel {
    @NotNull
    public String token;
    @NotNull
    @Email
    public String email;

}
