package com.documine.web.webmodels.response.classification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClassificationResultModel {

    @JsonProperty("metrics")
    public String metrics;

    @JsonProperty("guesses")
    public List<ClassAndConfidenceModel> classificationResult;

    @JsonProperty("content_to_classify_snippet")
    public String inputSnippet;

    @JsonProperty("best_guess")
    public ClassAndConfidenceModel bestGuess;

}


