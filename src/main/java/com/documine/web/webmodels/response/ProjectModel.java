package com.documine.web.webmodels.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectModel {

    public Long id;
    public String title;
    public String description;

}