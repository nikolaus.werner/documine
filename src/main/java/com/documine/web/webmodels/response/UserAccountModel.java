package com.documine.web.webmodels.response;

import com.documine.web.authentication.Grant;
import com.documine.web.authentication.Role;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountModel {

    private String email;
    private Date createdAt;
    private Role role;
    private List<Grant> resourceAccess;
    private boolean verified = false;
    private List<ProjectModel> projects = new ArrayList<>();

    public String getEmail() {
        return email;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Role getRole() {
        return role;
    }

    public List<ProjectModel> getProjects() {
        return projects;
    }

    public boolean isVerified() {
        return verified;
    }

    public List<Grant> getResourceAccess() {
        return this.resourceAccess;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setCreatedAt(final Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setProjects(final List<ProjectModel> projects) {
        this.projects = projects;
    }

    public void setResourceAccess(final List<Grant> resourceAccess) {
        this.resourceAccess = resourceAccess;
    }

}
