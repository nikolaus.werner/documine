package com.documine.web.webmodels.response.classification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

// meta model for instances that can be classified according to
// many metrics
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MultiClassificationResultModel {

    @JsonProperty("results_of_multi_classification")
    public List<ClassificationResultModel> classificationResultModels;

    @JsonProperty("content_to_classify_snippet")
    public String inputSnippet;

    // for JSON serialization
    public MultiClassificationResultModel() {
    }

    public MultiClassificationResultModel(final List<ClassificationResultModel> classificationResultModels) {
        this.classificationResultModels = classificationResultModels;
    }

}


