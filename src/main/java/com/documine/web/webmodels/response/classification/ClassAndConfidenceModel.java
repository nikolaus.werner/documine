package com.documine.web.webmodels.response.classification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClassAndConfidenceModel {

    @JsonProperty("class")
    public String classFound;

    @JsonProperty("confidence")
    public double confidence;

    // for JSON serialization
    public ClassAndConfidenceModel() {
    }

    public ClassAndConfidenceModel(final String classFound, final double confidence) {
        this.confidence = confidence;
        this.classFound = classFound;
    }

}


