package com.documine.web.webmodels.response;

public class SuccessModel {
    private String message = "success";

    // needed for json serialization
    public SuccessModel() {
    }

    public SuccessModel(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
