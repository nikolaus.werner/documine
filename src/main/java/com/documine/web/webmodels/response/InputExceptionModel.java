package com.documine.web.webmodels.response;

import com.documine.exception.InputException;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class InputExceptionModel implements ExceptionModel {


    private Map<String, String> fields;
    private String code;
    private String message;

    public InputExceptionModel(final InputException ex) {
        this.code = ex.getCode();
        this.message = ex.getMessage();
        this.fields = ex.getFields();
    }

    public InputExceptionModel(final Map<String, String> fields) {
        this.fields = fields;
        this.message = "invalid fields";
        this.code = "invalid_fields";
    }

    public String getMessage() {
        return this.message;
    }

    public String getCode() {
        return this.code;
    }

    @JsonProperty("invalid_fields")
    public Map<String, String> getInvalidFields() {
        return fields;
    }
}
