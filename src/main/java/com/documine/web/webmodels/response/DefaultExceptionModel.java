package com.documine.web.webmodels.response;

import com.documine.exception.DocumineException;

public class DefaultExceptionModel implements ExceptionModel {


    public String message;
    public String code;

    public DefaultExceptionModel(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public DefaultExceptionModel(final DocumineException ex) {
        this.code = ex.getCode();
        this.message = ex.getMessage();
    }

}
