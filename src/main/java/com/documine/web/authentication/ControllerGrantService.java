package com.documine.web.authentication;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ControllerGrantService {


    private static final Map<String, Map<String, Grant>> CONTROLLER_GRANT_MAP = new HashMap<String, Map<String, Grant>>();

    static {
        final Map<String, Grant> authActionGrant = new HashMap<>();
        authActionGrant.put("login", Grant.LOGIN);
        authActionGrant.put("logout", Grant.LOGOUT);
        authActionGrant.put("signup", Grant.SIGNUP);
        authActionGrant.put("verifywithpost", Grant.VERIFY_EMAIL);
        authActionGrant.put("verifywithget", Grant.VERIFY_EMAIL);
        CONTROLLER_GRANT_MAP.put("authentication", authActionGrant);

        final Map<String, Grant> userActionGrant = new HashMap<>();
        userActionGrant.put("showme", Grant.GET_ME);
        CONTROLLER_GRANT_MAP.put("useraccount", userActionGrant);

        final Map<String, Grant> classifyActionGrant = new HashMap<>();
        classifyActionGrant.put("guesslanguage", Grant.USE_DEMO_PROJECTS);
        classifyActionGrant.put("guessuseragent", Grant.USE_DEMO_PROJECTS);
        CONTROLLER_GRANT_MAP.put("classification", classifyActionGrant);

        final Map<String, Grant> projectActionGrant = new HashMap<>();
        projectActionGrant.put("create", Grant.POST_PROJECT);
        projectActionGrant.put("showMine", Grant.GET_PROJECT);
        projectActionGrant.put("showAllMine", Grant.GET_PROJECT);
        CONTROLLER_GRANT_MAP.put("project", projectActionGrant);


    }

    public Grant getControllerGrant(final String controllerName, final String actionName) {
        if (CONTROLLER_GRANT_MAP.containsKey(controllerName) && CONTROLLER_GRANT_MAP.get(controllerName).containsKey(actionName))
            return CONTROLLER_GRANT_MAP.get(controllerName).get(actionName);
        return null;
    }

}
