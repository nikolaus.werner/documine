package com.documine.web.authentication;

import java.util.ArrayList;
import java.util.List;

public enum Role {

    ANONYMOUS(
            Grant.SIGNUP,
            Grant.LOGIN,
            Grant.VERIFY_EMAIL),

    USER(
            Grant.GET_ME,
            Grant.LOGIN,
            Grant.LOGOUT,
            Grant.VERIFY_EMAIL),

    VERIFIED_USER(
            Grant.GET_ME,
            Grant.LOGIN,
            Grant.LOGOUT,
            Grant.USE_DEMO_PROJECTS,
            Grant.POST_PROJECT,
            Grant.GET_PROJECT),

    ADMIN(
            Grant.GET_ME,
            Grant.LOGIN,
            Grant.LOGOUT,
            Grant.USE_DEMO_PROJECTS,
            Grant.POST_PROJECT,
            Grant.GET_PROJECT);

    private List<Grant> grants = new ArrayList<>();

    private Role(final Grant... grants) {
        for (Grant grant : grants) {
            this.grants.add(grant);
        }
    }

    public boolean hasGrant(Grant grant) {
        return this.grants.contains(grant);
    }

    public List<Grant> getGrants() {
        return this.grants;
    }
}
