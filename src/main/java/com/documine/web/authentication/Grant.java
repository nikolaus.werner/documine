package com.documine.web.authentication;

public enum Grant {

    // authentication related
    LOGIN,
    LOGOUT,
    SIGNUP,
    VERIFY_EMAIL,

    // user account related
    GET_ME,

    // project related
    POST_PROJECT,
    GET_PROJECT,

    // demo project related
    USE_DEMO_PROJECTS;


}
