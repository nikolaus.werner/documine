package com.documine.exception;

import org.springframework.http.HttpStatus;

public abstract class DocumineException extends Exception {

    public DocumineException() {
        super();
    }

    public DocumineException(final String msg) {
        super(msg);
    }

    public abstract HttpStatus getStatusCode();

    public abstract String getCode();
}
