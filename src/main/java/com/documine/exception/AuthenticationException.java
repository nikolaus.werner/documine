package com.documine.exception;

import org.springframework.http.HttpStatus;

public class AuthenticationException extends DocumineException {

    private static final HttpStatus STATUS_CODE = HttpStatus.UNAUTHORIZED;
    private String code;

    public AuthenticationException() {
        super("unauthorized");
        this.code = "unauthorized";
    }


    @Override
    public HttpStatus getStatusCode() {
        return STATUS_CODE;
    }

    @Override
    public String getCode() {
        return code;
    }
}
