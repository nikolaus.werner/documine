package com.documine.exception;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class InputException extends DocumineException {

    private static final HttpStatus STATUS_CODE = HttpStatus.UNPROCESSABLE_ENTITY;
    private Map<String, String> fields;
    private String code = "invalid_input";

    public InputException() {
        super("invalid input fields");
    }

    public InputException(final String fieldName, final String message) {
        super("invalid input fields");
        this.fields = Maps.newHashMap(ImmutableMap.of(fieldName, message));
    }

    public InputException(final Map<String, String> fields) {
        super("invalid input fields");
        this.fields = fields;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public HttpStatus getStatusCode() {
        return STATUS_CODE;
    }

    public Map<String, String> getFields() {
        return fields;
    }
}
