package com.documine.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends DocumineException {

    private static final HttpStatus STATUS_CODE = HttpStatus.FORBIDDEN;
    private String code;

    public ForbiddenException() {
        super("forbidden");
        this.code = "forbidden";
    }

    @Override
    public HttpStatus getStatusCode() {
        return STATUS_CODE;
    }

    @Override
    public String getCode() {
        return code;
    }
}
