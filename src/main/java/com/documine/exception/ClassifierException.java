package com.documine.exception;

import org.springframework.http.HttpStatus;

public class ClassifierException extends DocumineException {

    private static final HttpStatus STATUS_CODE = HttpStatus.INTERNAL_SERVER_ERROR;
    private String code = "classifier_fail";

    public ClassifierException() {
        super("classifier_fail");
    }

    public ClassifierException(final String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode() {
        return STATUS_CODE;
    }

    @Override
    public String getCode() {
        return code;
    }
}
