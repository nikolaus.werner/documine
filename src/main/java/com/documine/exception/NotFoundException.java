package com.documine.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends DocumineException {

    private static final HttpStatus STATUS_CODE = HttpStatus.NOT_FOUND;
    private String code = "not_found";

    public NotFoundException() {
        super("not found");
    }

    public NotFoundException(final String msg) {
        super(msg);
    }

    public NotFoundException(final String msg, final String code) {
        super(msg);
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public HttpStatus getStatusCode() {
        return STATUS_CODE;
    }

}
