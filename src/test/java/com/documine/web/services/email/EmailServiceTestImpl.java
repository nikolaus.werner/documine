package com.documine.web.services.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class EmailServiceTestImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceTestImpl.class);

    @Value("${mail.verification.page}")
    private String verificationAddress;

    // no emails send when tests running
    @Override
    public void sendVerificationMail(final String email, final String verificationToken) {
        LOGGER.info("Test mode - verfication mail link: " + verificationAddress+"/"+verificationToken);
    }

}
