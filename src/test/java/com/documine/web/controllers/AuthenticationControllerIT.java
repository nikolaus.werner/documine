package com.documine.web.controllers;

import com.documine.DBIntegrationTestService;
import com.documine.web.Constants;
import com.documine.web.webmodels.request.AuthenticationModel;
import com.documine.web.webmodels.request.VerificationModel;
import com.documine.web.webmodels.response.SuccessModel;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static com.documine.TestHelpers.asJSONObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthenticationControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DBIntegrationTestService dbIntegrationTestService;

    private static final String VALID_EMAIL = "email1@email.com";
    private static final String PASSWORD = "abcd";
    private static final String ALREADY_EXISTING_EMAIL = "email2@email.com";
    private static final String INVALID_EMAIL = "emailemail.com";
    private static final String VERIFIED_USER_EMAIL = "verified@email.com";


    @Before
    public void setUp() {
        dbIntegrationTestService.cleanDB();
        dbIntegrationTestService.addUserAccount(ALREADY_EXISTING_EMAIL, PASSWORD);
        dbIntegrationTestService.addUserAccount(VERIFIED_USER_EMAIL, PASSWORD, true);

    }

    @After
    public void tearDown() {
        dbIntegrationTestService.cleanDB();
    }


    @Test
    public void POST_signup() {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = "aPasswordXyZ";
        reqBody.email = VALID_EMAIL;

        final ResponseEntity<SuccessModel> responseEntity =
                restTemplate.postForEntity("/signup", reqBody, SuccessModel.class);

        final SuccessModel body = responseEntity.getBody();
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals("success", body.getMessage());
        assertTrue(responseEntity.getHeaders().containsKey(Constants.X_AUTH_TOKEN));
    }

    @Test
    public void POST_signup_invalid_json() throws JSONException {
        final String invalidJsonStr = "{"
                + "\"email\":\"bla@mail.com\""
                + "\"password\":\"bla\""
                + "}";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        final HttpEntity request = new HttpEntity(invalidJsonStr, headers);

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/signup", request, Map.class);

        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
        assertEquals("invalid json", json.get("message"));
    }

    @Test
    public void POST_signup_invalid_email() throws JSONException {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = "aPasswordXyZ";
        reqBody.email = INVALID_EMAIL;

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/signup", reqBody, Map.class);

        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
        assertEquals("invalid fields", json.get("message"));
        assertTrue(((JSONObject) json.get("invalid_fields")).has("email"));
    }


    @Test
    public void POST_signup_duplicate_email() throws JSONException {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = PASSWORD;
        reqBody.email = ALREADY_EXISTING_EMAIL;

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/signup", reqBody, Map.class);

        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
        assertEquals("invalid input fields", json.get("message"));
        assertTrue(((JSONObject) json.get("invalid_fields")).has("email"));
    }

    @Test
    public void POST_login() {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = PASSWORD;
        reqBody.email = ALREADY_EXISTING_EMAIL;

        final ResponseEntity<SuccessModel> responseEntity =
                restTemplate.postForEntity("/login", reqBody, SuccessModel.class);

        final SuccessModel body = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("success", body.getMessage());
        assertTrue(responseEntity.getHeaders().containsKey(Constants.X_AUTH_TOKEN));
    }

    @Test
    public void POST_login_unknown_user() throws JSONException {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = PASSWORD;
        reqBody.email = ALREADY_EXISTING_EMAIL + "abc";

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/login", reqBody, Map.class);

        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals("unknown user", json.get("message"));
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void POST_login_wrong_password() throws JSONException {
        final AuthenticationModel reqBody = new AuthenticationModel();
        reqBody.password = PASSWORD + "abc";
        reqBody.email = ALREADY_EXISTING_EMAIL;

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/login", reqBody, Map.class);
        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals("unauthorized", json.get("message"));
        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
    }

    @Test
    public void POST_verify() throws JSONException {
        final VerificationModel reqBody = new VerificationModel();
        reqBody.token = dbIntegrationTestService.userAccounts.get(ALREADY_EXISTING_EMAIL).getVerificationToken();
        reqBody.email = ALREADY_EXISTING_EMAIL;

        final ResponseEntity<SuccessModel> responseEntity =
                restTemplate.postForEntity("/verify", reqBody, SuccessModel.class);
        final SuccessModel respBody = responseEntity.getBody();
        assertEquals("success", respBody.getMessage());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void POST_verify_unknown_email() throws JSONException {
        final VerificationModel reqBody = new VerificationModel();
        reqBody.token = dbIntegrationTestService.userAccounts.get(ALREADY_EXISTING_EMAIL).getVerificationToken();
        reqBody.email = "unknown@email.com";

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/verify", reqBody, Map.class);
        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals("email not found", json.get("message"));
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void POST_verify_unknown_token() throws JSONException {
        final VerificationModel reqBody = new VerificationModel();
        reqBody.token = "1234";
        reqBody.email = "unknown@email.com";

        final ResponseEntity<Map> responseEntity =
                restTemplate.postForEntity("/verify", reqBody, Map.class);
        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals("email not found", json.get("message"));
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void POST_verify_already_verified() throws JSONException {
        final VerificationModel reqBody = new VerificationModel();
        reqBody.token = dbIntegrationTestService.userAccounts.get(VERIFIED_USER_EMAIL).getVerificationToken();
        reqBody.email = VERIFIED_USER_EMAIL;

        final ResponseEntity<SuccessModel> responseEntity =
                restTemplate.postForEntity("/verify", reqBody, SuccessModel.class);
        final SuccessModel respBody = responseEntity.getBody();
        assertEquals("already verified", respBody.getMessage());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}
