package com.documine.web.controllers;

import com.documine.classification.ClassifierContainer;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class WekaFilterTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WekaFilterTest.class);

    private ClassifierContainer classifier;

    private ClassifierContainer getClassifier() throws IOException, ClassNotFoundException {
        return new ClassifierContainer(
                "src/main/resources/lgClassification/LanguageTraining_2gram_f300.arff",
                "src/main/resources/lgClassification/logisticClassifier_v1.model",
                "src/main/resources/lgClassification/CharNGramTokenizer",
                "language",
                "language");

    }

    private Instance toInstances(final String testSample) {

        final List<Attribute> attributes = createAttributes();
        final Attribute textAttribute = attributes.get(0);
        final Attribute classAttribute = attributes.get(1);
        final Instances dataset = createDataSet(attributes, classAttribute);
        final Instance raw = new DenseInstance(2);
        raw.setDataset(dataset);
        raw.setValue(textAttribute, textAttribute.addStringValue(testSample)); // this is the way to set string attribute values
        raw.setValue(classAttribute, "?");
        //raw.setClassMissing();
        LOGGER.info("First attribute: " + raw.attribute(0));
        LOGGER.info("First attribute value: " + raw.stringValue(0));
        LOGGER.info("Class attribute: " + raw.classAttribute());
        LOGGER.info("First text class value: " + raw.stringValue(1));

        return raw;
    }

    private List<Attribute> createAttributes() {
        // Create nominal attribute "position"
        final Attribute textAttribute = new Attribute("text", (ArrayList) null); // WTF!!! this is the way WEKA defines this as a string attribute
        final Attribute classAttribute = new Attribute("class", Lists.newArrayList("?"));
        return Lists.newArrayList(textAttribute, classAttribute);
    }

    private Instances createDataSet(final List<Attribute> attributes, final Attribute classAttribute) {
        // TODO: WTF - why is an array list expected here !!!
        final Instances dataset = new Instances("dataset", (ArrayList) attributes, 0);
        dataset.setClassIndex(classAttribute.index());
        return dataset;
    }

    private StringToWordVector getFilter() throws IOException, ClassNotFoundException {
        try (final FileInputStream fis = new FileInputStream("src/main/resources/lgClassification/CharNGramTokenizer")) {
            final StringToWordVector filter = (StringToWordVector) (new ObjectInputStream(fis)).readObject();
            return filter;
        }
    }

    private Instance rawToVector(final Instance raw) throws Exception {
        final StringToWordVector filter = getFilter();
        filter.setAttributeIndices("first");
        filter.setInputFormat(raw.dataset());
        Instances inst = new Instances(raw.dataset(), 1);
        inst.add(raw);
        Instances filtered = Filter.useFilter(inst, filter);
        return filtered.get(0);
    }

    private Instance makeClassifierCompatible(final Instance incompatible) {

        final Instance compatibleInstance = new DenseInstance(classifier.getNumAttributes());

        final Enumeration<Attribute> attributes = incompatible.enumerateAttributes();
        while (attributes.hasMoreElements()) {
            final Attribute currentAttribute = attributes.nextElement();
            final Attribute textAttribute = classifier.getAttribute(currentAttribute.name());

            if (textAttribute != null) {
                final double currentAttrValue = incompatible.value(currentAttribute);
                compatibleInstance.setValue(textAttribute, currentAttrValue);
            }
        }
        return compatibleInstance;
    }

}
