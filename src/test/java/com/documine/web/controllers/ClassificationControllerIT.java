package com.documine.web.controllers;

import com.documine.DBIntegrationTestService;
import com.documine.web.webmodels.request.ClassificationModel;
import com.documine.web.webmodels.response.classification.ClassificationResultModel;
import com.documine.web.webmodels.response.classification.MultiClassificationResultModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static com.documine.TestHelpers.xAuthHeaders;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClassificationControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DBIntegrationTestService dbService;

    private static final String PASSWORD = "abcd";
    private static final String VERIFIED_USER_EMAIL = "verified@email.com";


    @Before
    public void setUp() {
        dbService.cleanDB();
        dbService.addUserAccount(VERIFIED_USER_EMAIL, PASSWORD, true);

    }

    @After
    public void tearDown() {
        dbService.cleanDB();
    }


    @Test
    public void POST_classify_language() {
        final String token = dbService.authTokens.get(VERIFIED_USER_EMAIL);
        final ClassificationModel reqBody = new ClassificationModel();
        final String content = "I'm a not an second class item!";
        reqBody.contentToClassify = content;

        final HttpEntity requestEntity = new HttpEntity(reqBody, xAuthHeaders(token));
        final ResponseEntity<ClassificationResultModel> responseEntity =
                restTemplate.exchange("/classify/language", HttpMethod.POST, requestEntity, ClassificationResultModel.class);

        final ClassificationResultModel body = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(content.length() < 40 ? content : content.substring(0, 40) + "...", body.inputSnippet);
        assertTrue(8 <= body.classificationResult.size());
        assertEquals("English", body.classificationResult.get(0).classFound);
    }

    @Test
    public void POST_classify_user_agents() {
        final String token = dbService.authTokens.get(VERIFIED_USER_EMAIL);
        final ClassificationModel reqBody = new ClassificationModel();
        final String content = "Mozilla/4.0 (compatible; MSIE 8.0*; *Windows NT 5.0; *WOW64*Trident/4.0*)* 360Spider/";
        reqBody.contentToClassify = content;

        final HttpEntity requestEntity = new HttpEntity(reqBody, xAuthHeaders(token));
        final ResponseEntity<MultiClassificationResultModel> responseEntity =
                restTemplate.exchange("/classify/useragent", HttpMethod.POST, requestEntity, MultiClassificationResultModel.class);

        final MultiClassificationResultModel body = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(content.length() < 40 ? content : content.substring(0, 40) + "...", body.inputSnippet);
        assertTrue(3 == body.classificationResultModels.size());

        //assertEquals("English", body.getClassificationResult().get(0).getClassFound());
    }

}
