package com.documine.web.controllers;

import com.documine.DBIntegrationTestService;
import com.documine.web.authentication.Role;
import com.documine.web.webmodels.response.UserAccountModel;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static com.documine.TestHelpers.asJSONObject;
import static com.documine.TestHelpers.xAuthHeaders;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserAccessControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DBIntegrationTestService dbService;

    private static final String EMAIL = "email2@email.com";

    @Before
    public void setUp() {
        dbService.cleanDB();
        dbService.addUserAccount(EMAIL);
    }

    @After
    public void tearDown() {
        dbService.cleanDB();
    }


    @Test
    public void GET_me() {
        final String token = dbService.authTokens.get(EMAIL);
        final HttpEntity requestEntity = new HttpEntity(xAuthHeaders(token));
        final ResponseEntity<UserAccountModel> responseEntity =
                restTemplate.exchange("/me", HttpMethod.GET, requestEntity, UserAccountModel.class);

        final UserAccountModel body = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(EMAIL, body.getEmail());
        assertEquals(Role.USER, body.getRole());
        assertNotNull(body.getCreatedAt());
        assertTrue(body.getProjects().isEmpty());
        assertFalse(body.isVerified());

    }

    @Test
    public void GET_me_forbidden_no_token() throws JSONException {
        final ResponseEntity<Map> responseEntity =
                restTemplate.exchange("/me", HttpMethod.GET, new HttpEntity(new HttpHeaders()), Map.class);
        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertEquals("forbidden", json.get("message"));
    }

    @Test
    public void GET_me_forbidden_wrong_token() throws JSONException {
        final HttpEntity requestEntity = new HttpEntity(xAuthHeaders("UNKNOWN_TOKEN"));
        final ResponseEntity<Map> responseEntity =
                restTemplate.exchange("/me", HttpMethod.GET, new HttpEntity(new HttpHeaders()), Map.class);

        final JSONObject json = asJSONObject(responseEntity.getBody());
        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertEquals("forbidden", json.get("message"));
    }

}
