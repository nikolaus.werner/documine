package com.documine;

import com.documine.web.Constants;
import com.google.common.collect.Lists;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;

import java.util.List;
import java.util.Map;

public class TestHelpers {

    public static JSONObject asJSONObject(final Map<String, ?> body) {
        return new JSONObject(body);
    }

    public static JSONArray asJSONArray(final List<?> body) {
        return new JSONArray(body);
    }

    public static HttpHeaders xAuthHeaders(final String token) {
        final HttpHeaders headers = new HttpHeaders();
        headers.put(Constants.X_AUTH_TOKEN, Lists.newArrayList(token));
        return headers;
    }
}
