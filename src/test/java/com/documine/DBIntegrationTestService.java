package com.documine;

import com.documine.sql.models.UserAccount;
import com.documine.sql.repositories.AuthTokenRepository;
import com.documine.sql.repositories.UserAccountRepository;
import com.documine.web.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class DBIntegrationTestService {

    private static final String DEFAULT_PASSWORD = "thePassword";

    @Autowired
    private AuthTokenRepository authTokenRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;


    @Autowired
    private AuthenticationService authenticationService;

    public Map<String, UserAccount> userAccounts = new HashMap<>();
    public Map<String, String> userPasswords = new HashMap<>();
    public Map<String, String> authTokens = new HashMap<>();


    public void cleanDB() {
        authTokenRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    public DBIntegrationTestService addUserAccount() {
        final String defaultEmailAddress = "default" + UUID.randomUUID().toString() + "@mail.com";
        return addUserAccount(defaultEmailAddress);
    }

    public DBIntegrationTestService addUserAccount(final String email) {
        return addUserAccount(email, DEFAULT_PASSWORD);
    }

    public DBIntegrationTestService addUserAccount(final String email, final String password) {
        return addUserAccount(email, password, false);
    }

    public DBIntegrationTestService addUserAccount(final String email, final String password, final boolean emailVerified) {
        final UserAccount ua = new UserAccount(email);
        ua.setPassword(password);
        ua.setVerifiedEmail(emailVerified);
        userAccounts.put(email, userAccountRepository.save(ua));
        userPasswords.put(email, DEFAULT_PASSWORD);
        addToken(ua);
        return this;
    }

    private void addToken(final UserAccount userAccount) {
        final String token = authenticationService.createNewToken(userAccount);
        authTokens.put(userAccount.getEmail(), token);
    }

}
